# LEGAL GTCONNECTIONS


## Getting Started 🚀

Project for the automation of administrative processes for the web development area of ​​the company GTconnections.


### Prerequisites

It is necessary to have installed, the following software for the correct operation of the application.

```
Virtual server - Apache or equivalent
PHP - ver 7.3
Mysql Server or  equivalent

```

### Installing


For the installation of the application you can follow two different procedures that we will explain below:


1.-  Clone it from a gitlab repository

copy the following command line into your git console

```
git clone https://gitlab.com/jairantoniom/legales.git

```

Enter your password and github user, if required, if not then you can download a copy of the application for local use.

2.- Download directly

In the search bar of your preferred browser place the following url, which we will place next:

```
https://gitlab.com/jairantoniom/legales/-/archive/master/legales-master.zip

```


After this copy and decompress in the www or htdocs folder of your local server


## Built With

* [PHP](https://www.php.net/manual/es/intro-whatis.php)
* [MYSQL](https://www.mysql.com) 
* [jQuery](https://jquery.com)


## Versioning

We use [GIT ](https://git-scm.com) for versioning. For the versions available. 

## Authors

* **Jair Morillo** - *Initial work* - [jairmorillo](https://github.com/jairmorillo)

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details