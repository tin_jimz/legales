<?php
require_once("../autoload.php");
if(!isset($_SESSION["email"])) {
  redirect("../index.php");
}
$list = Query::list($pdo,'sentemail');
 ?>
<html lang="es">
<?php include_once '../inc/head.php'; ?>
<body class="hold-transition skin-black sidebar-mini">
<div class="wrapper">
  <!-- Main Header -->
  <?php include_once '../inc/nav-bar.php'; ?>
  <?php include_once '../inc/sidebar.php'; ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->

    <!-- Main content -->
    <section class="content container-fluid">
          <style> 
            .row {
                margin-right: 15px;
                margin-left: 15px;
              } 
          </style>
    <div class="row">
		<div class="">
              <a href="create.php" class="btn btn-primary"><span class="glyphicon glyphicon-plus"></span> Nuevo Registro</a>
              <br>
                <div class="box">
                    <div class="box-header">
                      <h3 class="box-title">Data Table With Full Features</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">

                    <table id="example1" class="table table-bordered table-striped">
                      <thead>
                        <tr>
                          <th scope="col">#</th>
                          <th scope="col">Nombre</th>
                          <th scope="col">Correo</th>
                          <th scope="col">Tipo/EM</th>    
                   
                        </tr>
                      </thead>
                      <tbody>
                          <?php foreach ($list as $key => $value):?>
                            <tr>

                              <th scope="row"><?= $value["id"] ?></th>
                              <td><?=$value["name"];?></td>
                              <td><?=$value["emailcostumer"];?></td>
                              <td><?=$value["typeemail"];?></td>                                                 

                            </tr>
                          <?php endforeach;?>
                      </tbody>
                  </table>
                  </div>
            </div>
          </div>
          </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <!-- Main Footer -->
  <?php include_once '../inc/main-footer.php'; ?>
  <!-- Add the sidebar's background. This div must be placed
  immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->
<?php include_once '../inc/footer.php'; ?>
<script>
  $('#example1').DataTable(); 
</script>
</body>
</html>