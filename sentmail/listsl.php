<?php
require_once("../autoload.php");
if(!isset($_SESSION["email"])) {
    redirect("../index.php");
}
if (isset($_POST["id"])) {
     $id = $_POST["id"];
     $list =  Query::searchId($pdo,'email',$id);
     $list2 = Query::searchAll($pdo,'email_var','id_email',$id);
     //dd($list2);
}
?>
<div class="ctn">
<div class="form-group">
  <label> Informacion para el correo:</label>            
</div>
<?php foreach ($list2 as $key => $value) : ?>
  <div class="form-group">
                <?php if($value["type"] =="text"){?> 
                        <?php foreach ($value as $key1 => $value2) : ?>
                          <?php if($key1 =="inputtitle"){?> 
                                    <label><?= $value2?>:</label> 
                                      <?php }if($key1 =="inputname" ){  ?>
                                    <input type="text" class="form-control"  name="<?= $value2?>" value=""   required>              
                              <?php } ?>  
                          <?php endforeach;?>
                <?php }if($value["type"] =="textarea" ){  ?>
                          <?php foreach ($value as $key1 => $value2) : ?>   
                            <?php if($key1 =="inputtitle"){?> 
                              <label><?= $value2?>:</label> 
                              <?php }if($key1 =="inputname" ){  ?>
                              <textarea  id="texteditor" name="<?= $value2?>"  class="form-control" rows="20" required></textarea>
                              <?php } ?>  
                            <?php endforeach;?>
                <?php }if($value["type"] =="number" ){  ?>
                              <?php foreach ($value as $key1 => $value2) : ?>
                                  <?php if($key1 =="inputtitle"){?> 
                                          <label><?= $value2?>:</label> 
                                        <?php }if($key1 =="inputname" ){  ?>
                                          <input type="number" class="form-control"  name="<?= $value2?>" value=""   required>              
                                    <?php } ?>  
                              <?php endforeach;?>
                <?php }if($value["type"] =="email" ){?>
                        <?php foreach ($value as $key1 => $value2) : ?>   
                          <?php if($key1 =="inputtitle"){?> 
                                  <label><?= $value2?>:</label> 
                                <?php }if($key1 =="inputname" ){  ?>
                                  <input type="email" class="form-control"  name="<?= $value2?>" value=""   required>              
                            <?php } ?>  
                          <?php endforeach;?>
                  <?php }if($value["type"] =="date" ){?>
                        <?php foreach ($value as $key1 => $value2) : ?>   
                          <?php if($key1 =="inputtitle"){?> 
                                  <label><?= $value2?>:</label> 
                                <?php }if($key1 =="inputname" ){  ?>
                                  <input type="date" class="form-control"  name="<?= $value2?>" value=""   required>              
                            <?php } ?>  
                          <?php endforeach;?>
                  <?php } ?> 
              </div>
                <script>
                
                  $(document).ready(function(){
            			tinymce.init({
                                selector: 'textarea#texteditor'
                              })
                        });
                  
                </script>
<?php endforeach;?> 
</div>
