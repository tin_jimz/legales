<?php
require_once("../autoload.php");
$select = Query::list($pdo,'email');  

if ($_POST){  
    //var_dump($_POST);
   //  exit;
   $con = Query::search($pdo,'email','id',$_POST["typeemail"]);
   $content =  $con['content'];
   $titlem = $con['title']; 

  $sent = new SentEmail($_POST["client"],$_POST["emailcostumer"],$titlem);

  $errores = $val->verifySentEmail($sent);
      if(count($errores)==0){    

         $email= $_POST["emailcostumer"];  

               $keys = array_keys($_POST);
               $values = array_values($_POST);

               $body= str_replace($keys, $values ,$content);
                
                  $head = 'MIME-Version: 1.0' . "\r\n";
                  $head .= 'Content-type: text/html; charset=utf-8' . "\r\n";
                  $head .= 'From: Gtconnections <non-reply@gtconnections.com>';

                 $sm = mail($email, $titlem, $body, $head);

                  BaseMYSQL::createSentEmail($pdo,$sent,'sentemail');
                   redirect ("list.php");  

      }
    }
?>
<html lang="es">
<?php include_once '../inc/head.php'; ?>
<body class="hold-transition skin-black sidebar-mini">
<div class="wrapper">
  <!-- Main Header -->
  <?php include_once '../inc/nav-bar.php'; ?>
  <?php include_once '../inc/sidebar.php'; ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <!-- Main content -->
    <section class="content container-fluid">       
     <div class="row">
        <!-- left column -->
        <div class="col-md-8">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h2 class="box-title">Formulario de registro de datos</h2>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <?php
                if(isset($errores)):?>
                  <ul class="alert alert-danger">
                    <?php
                    foreach ($errores as $key => $value) :?>
                      <li> <?=$value;?> </li>
                      <?php endforeach;?>
                  </ul>
                <?php endif;?>
            <form action="" method="POST" enctype= "multipart/form-data"   >
              <div class="box-body">
              <div class="form-group">
                   <label>Nombre destinatario:</label>   
                   <input name="client" type="text" id="client" class="form-control"  value="" placeholder="Nombre del Destinatario" required/>
                </div>
                <div class="form-group">
                   <label>Correo:</label>   
                   <input name="emailcostumer" type="email" id="emailcostumer" class="form-control"  value="" placeholder="Correo del cliente" required/>
                </div>
              <div class="form-group">
                       <label for="exampleInputFile">Tipo de Correo</label>
                        <select class="form-control" id="typeemail" name="typeemail"   required>
                        <option value =""  selected> Seleccione </option>

                          <?php
                          foreach ($select as $key => $value) :?>
                            <option value ="<?=$value['id'];?>" > <?=$value['title'];?> </option>
                          <?php endforeach;?>
                      </select>
                </div> 

                <div id="cont"></div> 
                
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
              <button class="btn-buttom btn-primary" type="submit">Enviar</button>
            
              <button  class="btn-buttom btn-success" type="reset">Restablecer</button>
              </div>
            </form>
          </div>
          <!-- /.box -->
        </div>
    
    </section>
    
   <!-- /.content -->
   </div>
  <!-- /.content-wrapper -->
  <!-- Main Footer -->
  <?php include_once '../inc/main-footer.php'; ?>
  <!-- Add the sidebar's background. This div must be placed
  immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->
<?php include_once '../inc/footer.php'; ?>


<script>
    $(document).ready(function(){
				$("#typeemail").change(function () {			
					id = $("#typeemail").val();
						$.post("listsl.php", { id: id }, function(data){
							$("#cont").html(data);
						});            
				
				})
			});
</script>
</body>
</html>