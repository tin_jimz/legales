<?php
class SentEmail{
    private $name;
    private $emailcostumer;
    private $typeemail;

    public function __construct($name,$emailcostumer,$typeemail){
        $this->name = $name;
        $this->emailcostumer = $emailcostumer;
        $this->typeemail = $typeemail;     
    }
    public function getName(){
        return $this->name;
    }
    public function setName($name){
        $this->name = $name;
    }

    public function getEmailcostumer(){
        return $this->emailcostumer;
    }
    public function setEmailcostumer($emailcostumer){
        $this->emailcostumer = $emailcostumer;
    }
    
    public function getTypeemail(){
        return $this->typeemail;
    }
    public function setTypeemail($typeemail){
        $this->typeemail = $typeemail;
    }
    
    
}
?>