<?php

class BaseMYSQL extends BaseDatos{
    static public function conexion($host,$db_nombre,$dbusers,$password,$puerto,$charset){
        try {
            $dsn = "mysql:host=".$host.";"."dbname=".$db_nombre.";"."port=".$puerto.";"."charset=".$charset;
            $db = new PDO($dsn,$dbusers,$password);
            $db->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);
            return $db;
        } catch (PDOException $errores) {
            echo "No me pude conectar a la BD ". $errores->getmessage();
            exit;
        }
    }

    
    static public function buscarPorEmail($email,$pdo,$tabla){
        $sql = "select * from $tabla where email = :email";
        $query = $pdo->prepare($sql);
        $query->bindValue(':email',$email);
        $query->execute();
        $users = $query->fetch(PDO::FETCH_ASSOC);
        return $users;
    }

    static public function guardarUsuario($pdo,$users,$tabla,$avatar){
        $sql = "insert into $tabla (name,email,password,avatar,role) values (:name,:email,:password,:avatar,:role )";
        $query = $pdo->prepare($sql);
        $query->bindValue(':name',$users->getName());
        $query->bindValue(':email',$users->getEmail());
        $query->bindValue(':password',Encriptar::hashPassword($users->getPassword()));
        $query->bindValue(':avatar',$avatar);
        $query->bindValue('role',1);
        $query->execute();

    }
  
    public function leer(){
        //A futuro trabajaremos en esto
    }
    public function actualizar(){
        //A futuro trabajaremos en esto
    }
    public function borrar(){
        //A futuro trabajaremos en esto
    }
    public function guardar($users){
        //Este fue el método usao para json
    }

      static public function createEmail($pdo,$email,$tabla){
        $sql = "insert into $tabla (title,content,autor,lenguage) values (:title,:content,:autor,:lenguage )";
        $query = $pdo->prepare($sql);
        $query->bindValue(':title',$email->getTitle());
        $query->bindValue(':content',$email->getContent());
        $query->bindValue(':autor',$email->getAutor());
        $query->bindValue(':lenguage',$email->getLenguage());
        $query->execute();       
        $em =  $pdo->lastInsertId();
        return $em;
    }

    static public function createInputs($pdo,$input,$tabla){
        $sql = "insert into $tabla (inputtitle,inputname,type,id_email) values (:title,:name,:type,:id_email)";
        $query = $pdo->prepare($sql);
        $query->bindValue(':title',$input->getTitle());
        $query->bindValue(':name',$input->getName());
        $query->bindValue(':type',$input->getType());
        $query->bindValue(':id_email',$input->getIdemail());
        $query->execute();
    }



    static public function createSentEmail($pdo,$sent,$tabla){
        $sql = "insert into $tabla (name,emailcostumer,typeemail) values (:name,:emailcostumer,:typeemail )";
        $query = $pdo->prepare($sql);
        $query->bindValue(':name',$sent->getName());
        $query->bindValue(':emailcostumer',$sent->getEmailcostumer());
        $query->bindValue(':typeemail',$sent->getTypeemail());
        $query->execute();

    }









} 