<?php
class Input{
    private $title;
    private $name;
    private $type;
    private $id_email;


    public function __construct($title,$name,$type,$id_email){
        
        $this->title = $title;  
        $this->name = $name;
        $this->type = $type;
        $this->id_email = $id_email;  
    }


    public function getTitle(){
        return $this->title;
    }
    public function setTitle($title){
        $this->title = $title;
    }
    public function getName(){
        return $this->name;
    }
    public function setName($name){
        $this->name = $name;
    } 

    public function getType(){
        return $this->type;
    }
    public function setType($type){
        $this->type = $type;
    } 

    public function getIdemail(){
        return $this->id_email;
    }
    public function setIdemail($id_email){
        $this->id_email = $id_email;
    }   
    
}
?>