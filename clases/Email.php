<?php
class Email{
    private $title;
    private $content;
    private $autor;
    private $lenguage;


    public function __construct($title,$content,$autor,$lenguage){
        $this->title = $title;
        $this->content = $content;
        $this->autor = $autor;    
        $this->lenguage = $lenguage;
 
    }
    public function getTitle(){
        return $this->title;
    }
    public function setTitle($title){
        $this->title = $title;
    }

    public function getContent(){
        return $this->content;
    }
    public function setContent($content){
        $this->content = $content;
    }
    
    public function getAutor(){
        return $this->autor;
    }
    public function setAutor($autor){
        $this->autor = $autor;
    }

    public function getLenguage(){
        return $this->lenguage;
    }
    public function setLenguage($lenguage){
        $this->lenguage = $lenguage;
    }
    
    
}
?>