<?php
class Query{
    static public function listUser($pdo,$tabla){
        $sql="select $tabla.id, $tabla.name, $tabla.email from $tabla";
        $consulta= $pdo->query($sql);
        $listado=$consulta->fetchall(PDO::FETCH_ASSOC);
        return $listado;
    }
    static public function showUsers($pdo,$tabla,$idUsuario){
        $sql = "select $tabla.id, $tabla.name, $tabla.email, $tabla.avatar,$tabla.role from $tabla where $tabla.id = '$idUsuario'";
        $query = $pdo->prepare($sql);
        $query->execute();
        $usuarioEncontrado=$query->fetchAll(PDO::FETCH_ASSOC);
        return $usuarioEncontrado;
    }
    static public function deleteUsers($pdo,$tabla,$idUsuario){
        $sql="delete from $tabla where $tabla.id=:id";
        $query=$pdo->prepare($sql);
        $query->bindValue(':id',$idUsuario);
        $query->execute();
    }
    static public function editUser($pdo,$tabla,$idUsuario){
        $sql = "select $tabla.id, $tabla.name, $tabla.email, $tabla.password, $tabla.role from $tabla where $tabla.id = '$idUsuario'";
        $query = $pdo->prepare($sql);
        $query->execute();
        $usuarioModificar=$query->fetch(PDO::FETCH_ASSOC);
        return $usuarioModificar;
    }

    /* Funciones genericas*/

    static public function list($pdo,$tabla){
        $sql="select * from $tabla";
        $consulta = $pdo->query($sql);
        $listar = $consulta->fetchall(PDO::FETCH_ASSOC);
        return $listar;
    }

    static public function search($pdo,$tabla,$campo,$valor){
        $sql="select * from $tabla where $campo = '$valor'";
        $consulta = $pdo->query($sql);
        $listar = $consulta->fetch(PDO::FETCH_ASSOC);
        return $listar;
    }


    static public function searchAll($pdo,$tabla,$campo,$valor){
        $sql="select * from $tabla where $campo = '$valor'";
        $consulta = $pdo->query($sql);
        $listar = $consulta->fetchall(PDO::FETCH_ASSOC);
        return $listar;
    }

    static public function searchId($pdo,$tabla,$id){
        $sql="select $tabla.* from $tabla where id = $id";
        $query = $pdo->query($sql);
        $query->execute();
        $listar = $query->fetchall(PDO::FETCH_ASSOC);
        return $listar;
    }
    
    static public function deleteRow($pdo,$tabla,$id){
        $sql="delete from $tabla where id=:id";
        $query=$pdo->prepare($sql);
        $query->bindValue(':id',$id);
        $query->execute();
    }

    


}

?>