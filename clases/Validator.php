<?php
class Validator{

    public function validationUsers($users){
        
        $errores=array();
        $name = trim($users->getName());
        if(isset($name)) {
            if(empty($name)){
                $errores["name"]= "El campo nombre no debe estar vacio";
            }
        }
    
        $email = trim($users->getEmail());
        if(!filter_var($email, FILTER_VALIDATE_EMAIL)){
            $errores["email"]="Email invalido !!!!!";
        }
        $password= trim($users->getPassword());
   
        $repassword = trim($users->getRepassword());
        

        if(empty($password)){
            $errores["password"]= "Hermano querido el campo password no lo podés dejar en blanco";
        }elseif (strlen($password)<6) {
            $errores["password"]="La contraseña debe tener como mínimo 6 caracteres";
        }
        if(isset($repassword)){
            if ($password != $repassword) {
                $errores["repassword"]="Las contraseñas no coinciden";
            }
        }
        if($users->getAvatar()!=null){
            if($_FILES["avatar"]["error"]!=0){
                $errores["avatar"]="Error debe subir imagen";
            }else{
                $name = $_FILES["avatar"]["name"];
                $ext = pathinfo($name,PATHINFO_EXTENSION);
                if($ext != "png" && $ext != "jpg"){
                    $errores["avatar"]="Debe seleccionar archivo png ó jpg";
                }
            }
        }
    
        return $errores;
    }
    //Metodo creado para validar el login del usuario
    public function validacionLogin($users){
        $errores=array();
    
        $email = trim($users->getEmail());
        if(!filter_var($email, FILTER_VALIDATE_EMAIL)){
            $errores["email"]="Email invalido !!!!!";
        }
        $password= trim($users->getPassword());
       
        if(empty($password)){
            $errores["password"]= "Hermano querido el campo password no lo podés dejar en blanco";
        }elseif (strlen($password)<6) {
            $errores["password"]="La contraseña debe tener como mínimo 6 caracteres";
        }
    
        return $errores;
    }
    //Método para validar si el usuario desea recuperar su contraseña
    public function validacionOlvide($users){
        
        $errores=array();    
        $email = trim($users->getEmail());
        if(!filter_var($email, FILTER_VALIDATE_EMAIL)){
            $errores["email"]="Email invalido !!!!!";
        }

        $password= trim($users->getPassword());   
        $repassword = trim($users->getRepassword());
        
        if(empty($password)){
            $errores["password"]= "Hermano querido el campo password no lo podés dejar en blanco";
        }elseif (strlen($password)<6) {
            $errores["password"]="La contraseña debe tener como mínimo 6 caracteres";
        }
        if(empty($repassword)){
            $errores["repassword"]= "Hermano querido el campo confirmar nuevo password no lo podés dejar en blanco";
        }
            return $errores;
    }



    public function verifyEmail($email){
        
        $errores=array();

        $title = trim($email->getTitle());

        if(isset($title)) {
            if(empty($title)){
                $errores["title"]= "El campo titulo no debe estar vacio";
            }
        }
    
        $content = trim($email->getContent());

        if(isset($content)) {
            if(empty($content)){
                $errores["content"]= "El campo contenido no debe estar vacio";
            }
        }
    

        $autor = trim($email->getAutor());

        if(isset($autor)) {
            if(empty($autor)){
                $errores["autor"]= "El campo autor no debe estar vacio";
            }
        }
  
    
        return $errores;
    }

    public function verifySentEmail($sent){
        
        $errores=array();

        $name = trim($sent->getName());

        if(isset($name)) {
            if(empty($name)){
                $errores["name"]= "El campo nombre no debe estar vacio";
            }
        }
    
        $emailcostumer = trim($sent->getEmailcostumer());

        if(isset($emailcostumer)) {
            if(empty($emailcostumer)){
                $errores["emailcostumer"]= "El campo correo no debe estar vacio";
            }
        }
    

        $typeemail = trim($sent->getTypeemail());

        if(isset($typeemail)) {
            if(empty($typeemail)){
                $errores["typeemail"]= "El campo Tipo de correo no debe estar vacio";
            }
        }
  
    
        return $errores;
    }

    public function verifyInput($input){
        
        $errores=array();

        $name = trim($input->getName());

        if(isset($name)) {
            if(empty($name)){
                $errores["name"]= "El campo nombre  no debe estar vacio";
            }
        }
    
        $type = trim($input->getType());

        if(isset($type)) {
            if(empty($type)){
                $errores["type"]= "El campo correo no debe estar vacio";
            }
        }
    

        $id_email = trim($input->getIdemail());

        if(isset($id_email)) {
            if(empty($id_email)){
                $errores["id_email"]= "El campo Tipo de correo no debe estar vacio";
            }
        }
  
    
        return $errores;
    }


}

