<?php
require_once("../autoload.php");
if ($_POST){
  $tipoConexion = "MYSQL";
  if($tipoConexion=="JSON"){
    $users = new Users($_POST["email"],$_POST["password"],$_POST["repassword"],$_POST["name"],$_FILES );  
    $errores = $val->validationUsers($users, $_POST["repassword"]);    
    
    if(count($errores)==0){
      $usersF = $json->buscarEmail($usersF->getEmail());
      if($usersF != null){
        $errores["email"]="Usuario ya registrado";
      }else{
        $avatar = $reg->armarAvatar($users->getAvatar());
        $reg = $reg->armarUsuario($users,$avatar);
        $json->guardar($reg);      
               redirect ("list.php");
      }
    }
  }
 else{
 
  $users = new Users($_POST["email"],$_POST["password"],$_POST["repassword"],$_POST["name"],$_FILES );

  $errores = $val->validationUsers($users, $_POST["repassword"]);

  if(count($errores)==0){
    $usersF = BaseMYSQL::buscarPorEmail($users->getEmail(),$pdo,'users');
    if($usersF != false){
      $errores["email"]= "Usuario ya Registrado";
    }else{

      $avatar = $reg->armarAvatar($users->getAvatar());

      BaseMYSQL::guardarUsuario($pdo,$users,'users',$avatar);

      redirect ("list.php");
    }
  }

 } 
}

?>
<html lang="es">
<?php include_once '../inc/head.php'; ?>
<body class="hold-transition skin-black sidebar-mini">
<div class="wrapper">
  <!-- Main Header -->
  <?php include_once '../inc/nav-bar.php'; ?>
  <?php include_once '../inc/sidebar.php'; ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <!-- Main content -->
    <section class="content container-fluid">       
     <div class="row">
        <!-- left column -->
        <div class="col-md-6">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h2 class="box-title">Formulario de registro de datos</h2>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <?php
                if(isset($errores)):?>
                  <ul class="alert alert-danger">
                    <?php
                    foreach ($errores as $key => $value) :?>
                      <li> <?=$value;?> </li>
                      <?php endforeach;?>
                  </ul>
                <?php endif;?>
            <form action="" method="POST" enctype= "multipart/form-data"   >
              <div class="box-body">
              
                <div class="form-group">
                  <label> Usuario:</label>            
                   <input name="name" type="text" id="name" class="form-control"  value="<?=(isset($errores["name"]) )? "" : inputUsuario("name");?>" placeholder="Nombre de usuario..." />
                </div>
                <div class="form-group">
                     <label>Email:</label>
                      <input name="email" type="text" id="email" class="form-control"  value="<?=isset($errores["email"])? "":inputUsuario("email") ;?>" placeholder="Correo electrónico"/>
                </div>
                <div class="form-group">
                   <label>Contraseña:</label>          
                    <input name="password" type="password" id="password"  class="form-control"value="" placeholder="Contraseña..." />      
                </div>
                <div class="form-group">
                    <label>Confirmar contraseña:</label>            
                    <input name="repassword" type="password" id="repassword" class="form-control" value="" placeholder="Rectifique su contraseña" />
                </div>
                <div class="form-group">
                  <label for="exampleInputFile">Avatar</label>
                  <input  type="file" class="form-control" name="avatar" value=""/>
                </div>               
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
              <button class="btn-buttom btn-primary" type="submit">Enviar</button>
            
              <button  class="btn-buttom btn-success" type="reset">Restablecer</button>
              </div>
            </form>
          </div>
          <!-- /.box -->
        </div>













      
    </section>
   <!-- /.content -->
   </div>
  <!-- /.content-wrapper -->
  <!-- Main Footer -->
  <?php include_once '../inc/main-footer.php'; ?>
  <!-- Add the sidebar's background. This div must be placed
  immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->
<?php include_once '../inc/footer.php'; ?>

</body>
</html>
