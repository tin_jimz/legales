<?php
 include_once "../autoload.php";

if($_POST){

  $TypeCon = "MYSQL";

  if($TypeCon=="JSON"){
      $users = new Users($_POST["email"],$_POST["password"]);

      $errores= $val->validacionLogin($users);
      if(count($errores)==0){
      
        $usersF = $json->buscarPorEmail($users->getEmail());
        if($usersF == null){
          $errores["email"]="Usuario no existe";
        }else{
          if(Autenticador::verificarPassword($users->getPassword(),$usersF["password"] )!=true){
            $errores["password"]="Error en los datos verifique";
          }else{
            Autenticador::seteoSesion($usersF);
            if(isset($_POST["recordar"])){
              Autenticador::seteoCookie($usersF);
            }
            if(Autenticador::validarUsuario()){
              redirect("perfil.php");
            }else{
              redirect("registro.php");
            }
          }
        }
      }
  }else{
    
      $users = new Users($_POST["email"],$_POST["password"]);
      $errores= $val->validacionLogin($users);
      if(count($errores)==0){
        $usersF = BaseMYSQL::buscarPorEmail($users->getEmail(),$pdo,'users');
        if($usersF == false){
          $errores["email"]="Usuario no registrado";
        }else{
          if(Autenticador::verificarPassword($users->getPassword(),$usersF["password"] )!=true){
            $errores["password"]="Error en los datos verifique";
          }else{
            Autenticador::seteoSesion($usersF);
            if(isset($_POST["recordar"])){
              Autenticador::seteoCookie($usersF);
            }
            if(Autenticador::validarUsuario()){
              redirect("perfil.php");
            }else{
              redirect("registro.php");
            }
          }
        }
      }
  }
}
?>

<!DOCTYPE html>
<html lang="es">

<?php include_once '../inc/head.php'; ?>
<body lass="hold-transition login-page">
       <div class="col-md-12">
              <div class="col-xs-12 col-md-4" >
                <div class="login-box">
                      <div class="login-logo">
                      <a href="#"><img src="../imagenes/logogtconnections.png" class="logo" ></a>
                      </div>
                    <!-- /.login-logo -->
                    <div class="login-box-body">
                    <p class="login-box-msg">Ingresa tus datos para iniciar sesion</p>
                    <?php  if(isset($errores)){ ?>
                        <ul class="alert alert-danger">
                          <?php foreach ($errores as $key => $value) {?>
                            <li> <?php  echo $value; ?> </li>
                            <?php } ?>
                        </ul>
                      <?php }?>
                    <form action="" method="post" class="form_login">
                      <div class="form-group has-feedback">
                      <input name="email" class="form-control"  type="text" id="email"   value="<?=isset($errores["email"])? "":inputUsuario("email") ;?>" placeholder="Correo electrónico"/>
                      <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                      </div>
                      <div class="form-group has-feedback">
                    <input name="password"  class="form-control" type="password" id="password"  value="" placeholder="Contraseña..." />
                      <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                      </div>
                      <div class="row">
                      
                      <!-- /.col -->
                      <div class="col-xs-12">
                                  <button class="btn btn-primary btn-block btn-flat" type="submit">Entrar</button>

                      </div>
                      <!-- /.col -->
                      </div>
                    </form>
                    </div>
                  <!-- /.login-box-body -->
                </div>
                <!-- /.login-box -->
            </div>
            <div class="col-xs-12 col-md-8 media_login"></div>
   </div>
  <?php include_once '../inc/footer.php'; ?>
</body>
</html>
