<?php
require_once("../autoload.php");

if(!isset($_SESSION["email"])) {
  redirect("../index.php");
}
if (isset($_GET["id"])) {
  $id=$_GET["id"];
  $list =  Query::editUser($pdo,'users',$id);
}
if (isset($_POST["modificar"])) {
                  foreach ($_POST as $key => $value) {
                          if($key =='password' ){
                            $pass= Encriptar::hashPassword($value);
                            $sql="update users set $key='$pass' where users.id=:id";
                            $query=$pdo->prepare($sql); 
                            $query->bindValue(':id',$_POST['id']);
                            $query->execute();
                          }else{
                            $sql="update users set $key='$value' where users.id=:id";
                            $query=$pdo->prepare($sql); 
                            $query->bindValue(':id',$_POST['id']);
                            $query->execute();
                          } 
                  header('Location:list.php');
                  }
  } elseif (isset($_POST["no"])){
      header('Location:list.php');
      exit;
  }

 ?>
<html lang="es">
<?php include_once '../inc/head.php'; ?>
<body class="hold-transition skin-black sidebar-mini">
<div class="wrapper">
  <!-- Main Header -->
  <?php include_once '../inc/nav-bar.php'; ?>
  <?php include_once '../inc/sidebar.php'; ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <!-- Main content -->
    <section class="content container-fluid">       
     <div class="row">
        <!-- left column -->
        <div class="col-md-6">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h2 class="box-title">Formulario de registro de datos</h2>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <?php
                if(isset($errores)):?>
                  <ul class="alert alert-danger">
                    <?php
                    foreach ($errores as $key => $value) :?>
                      <li> <?=$value;?> </li>
                      <?php endforeach;?>
                  </ul>
                <?php endif;?>
    <form class="" action="" method="post">
    <div class="box-body">
        <?php foreach ($list as $key => $value) : ?>
            <div class="form-group">
                <label><?= $key?> :</label>
                      <?php if($key =="id"){?>
                          <input type="text" class="form-control"  disabled name="<?= $key?>" value="<?= $value?> ">
                      <?php }if($key =="password"){?>
                          <input type="password" class="form-control" disabled  id="password" name="<?= $key?>" value="<?= $value?>">
                      <?php }if($key != "id"){              
                          if($key != "password"){  ?>
                          <input type="text" class="form-control"  disabled name="<?= $key?>" value="<?= $value?> ">
                          <?php } ?>                                          
                      <?php }?>      
          <?php endforeach;?>
        </div>
      </div>
    <br>
    <div class="box-footer">
  
     </div> 
   </form>
   </div>
          <!-- /.box -->
        </div>

      
    </section>
   <!-- /.content -->
   </div>
  <!-- /.content-wrapper -->
  <!-- Main Footer -->
  <?php include_once '../inc/main-footer.php'; ?>
  <!-- Add the sidebar's background. This div must be placed
  immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->
<?php include_once '../inc/footer.php'; ?>

</body>
</html>

