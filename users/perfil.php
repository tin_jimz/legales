<?php
require_once("../autoload.php");
if(!isset($_SESSION["email"])) {
    redirect("../index.php");
}
?>
<html lang="es">
<?php include_once '../inc/head.php'; ?>
  <body class="hold-transition skin-black sidebar-mini">
    <div class="wrapper">
      <!-- Main Header -->
      <?php include_once '../inc/nav-bar.php'; ?>
        <?php include_once '../inc/sidebar.php'; ?>
        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
          <!-- Content Header (Page header) -->
          <!-- Main content -->
            <section class="content container-fluid">

            </section>
          <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->
        <!-- Main Footer -->
        <?php include_once '../inc/main-footer.php'; ?>
      <!-- Add the sidebar's background. This div must be placed
      immediately after the control sidebar -->
      <div class="control-sidebar-bg"></div>
    </div>
    <!-- ./wrapper -->
  <?php include_once '../inc/footer.php'; ?>
</body>
</html>