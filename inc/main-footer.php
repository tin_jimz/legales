<footer class="main-footer">
  <div class="container">
    <div class="pull-right hidden-xs">
      <b>Version</b> 0.0.1
    </div>
    <strong>Copyright &copy; 2019 <a href="#">Gtconnections</a>.</strong> All rights
    reserved.
  </div>
  <!-- /.container -->
</footer>