 <!-- Left side column. contains the logo and sidebar -->
 <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="../imagenes/<?=$_SESSION["avatar"];?>" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p><?=$_SESSION["name"];?></p>
          <!-- Status -->
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>   

      <!-- Sidebar Menu -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MENU</li>
        <!-- Optionally, you can add icons to the links -->
        <?php if($_SESSION["role"]==9):?>
            <li class="active"><a href="../users/list.php"><i class="fa  fa-user"></i> <span>Administración de Usuarios</span></a></li>
         <?php endif;?>    
           
            <li class="treeview" style="height: auto;">
              <a href="#">
                  <i class="fa fa-envelope"></i> <span>Documentos</span>
                  <span class="pull-right-container">
                      <i class="fa fa-angle-left pull-right"></i>
                  </span>
              </a>
                <ul class="treeview-menu" style="display: none;">
                    <li class="active"><a href="../legal/legal.php"><i class="fa fa-gavel"></i> <span>Terminos Legales</span></a></li>
                    <li class="active"><a href="../archives/index.php"><i class="fa fa-gavel"></i> <span>Propuestas</span></a></li>

                </ul>
         </li>



          <li class="treeview" style="height: auto;">
              <a href="#">
                  <i class="fa fa-envelope"></i> <span>Correos</span>
                  <span class="pull-right-container">
                      <i class="fa fa-angle-left pull-right"></i>
                  </span>
              </a>
                <ul class="treeview-menu" style="display: none;">
                  <li class="active"><a href="../emails/list.php"><i class="fa fa-envelope"></i> <span>Gestion de Correos</span></a></li>
                  <li class="active"><a href="../sentmail/list.php"><i class="fa fa-envelope"></i> <span>Envio de Correos</span></a></li>
                </ul>
         </li>
          

      
      </ul>
      <!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
  </aside>
