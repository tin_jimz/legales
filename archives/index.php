<?php
require_once("../autoload.php");
if(!isset($_SESSION["email"])) {
    redirect("index.php");
}
?>
<html lang="es">
<?php include_once '../inc/head.php'; ?>
<body class="hold-transition skin-black sidebar-mini">
<div class="wrapper">
  <!-- Main Header -->
  <?php include_once '../inc/nav-bar.php'; ?>
  <?php include_once '../inc/sidebar.php'; ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->

<style>
	.hides {
				display: none;
			}
</style>


    <!-- Main content -->
    <section class="content container-fluid">	
	<div class="content">
		<div class="row">
				<div class="col-md-4">
				</div>
				<div class="col-md-4">
					<img src="../imagenes/logogtconnections.png"  width=100%; />
						<form name="formulario" method="post" action="script.php">
						<div class="form-group">
							<label for="type">Dirigido a:</label>
							<select class="form-control"  name="type" id="type" required>
								<option value="person">Persona</option>						
								<option value="company">Compañia</option>
							</select>
						</div>
						<div class="form-group">
							<label for="typedoc">Tipo de propuesta</label>
							<select class="form-control"  name="doc" id="doc" required>
								<option value="1">Estandar</option>						
								<option value="2">Ecommerce</option>
								<option value="3">Personalizada</option>
								<option value="4">Ecommerce + SEO</option>
								<option value="5">SEO</option>
							</select>
						</div>
						
						<div class="form-group">
							<label for="name">Nombre empresa o Dueño:</label>
							<input type="text" class="form-control" name="name" value="" required>
						</div>

						<div class="form-group">
						    <br>
							<label for="name"><h3>Propuesta Economica y Metodos de pago</h3></label>
							<p>Debe introducirce el monto correspondiente a cada renglon en particular, </p>
							<br>
						 </div>
						<div id="div1">
												
						</div>
			
						<div class="form-group">
						    <br>
							<br>
						</div>						
						<div class="form-group">
							<label for="Lenguaje">Lenguaje</label>
							<select class="form-control"  name="lenguage" id="lenguage" required>
								<option value="eng">Ing</option>						
								<option value="esp">Esp</option>
							</select>
						</div>			 
							<input type="submit" value="Generar" class="btn btn-default" />
						</form>
				</div>
					<div class="col-md-4">
					</div>
	</div>
</div>
		
</section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <!-- Main Footer -->


  <?php include_once '../inc/main-footer.php'; ?>
  <!-- Add the sidebar's background. This div must be placed
  immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->
<?php include_once '../inc/footer.php'; ?>
<script>
			$(function(){
			
			$("#doc").on('change', function(){

				var selectValue = $(this).val();
				switch(selectValue) {

					case "1":
						$("div#div1").empty();
						$( "div#div1" ).append( '<div class="form-group"><label for="name">Diseño de la Plataforma  y lógica de registro:</label><input type="number" class="form-control" min="10"  max="9999" name="number1" value="" required></div>	<div class="form-group"><label for="name">Desarrollo de la Plataforma Web:</label><input type="number" class="form-control" min="10"  max="9999" name="number2" value="" required></div><div class="form-group"><label for="name">Despliegue de la Plataforma en el Servidor:</label><input type="number" class="form-control" min="10"  max="9999" name="number3" value="" required></div>' );
						break;

					case "2":
						$("div#div1").empty();
						$("div#div1" ).append( '<div class="form-group"><label for="name">Desarrollo de Tienda virtual:</label><input type="number" class="form-control" min="10"  max="9999" name="number1" value="" required></div><div class="form-group"><label for="name">Diseño personalizado:</label><input type="number" class="form-control" min="10"  max="9999" name="number2" value="" required></div><div class="form-group"><label for="name">Compatibilidad con todo dispositivo:</label><input type="number" class="form-control" min="10"  max="9999" name="number3" value="" required></div><div class="form-group"><label for="name">Metodos de pago</label><input type="number" class="form-control" min="10"  max="9999" name="number4" value="" required></div>' );
						break;

					case "3":
						$("div#div1").empty();
						$("div#div1" ).append( '<div class="form-group"><label for="name">Tipo de pagina</label><input type="text" class="form-control"  name="page" value="" required></div><div class="form-group"><label for="name">Diseño de logo básico y manual de uso:</label><input type="number" class="form-control" min="10"  max="9999" name="number4" value="" required></div><div class="form-group"><label for="name">Diseño de la Plataforma  y lógica de registro:</label><input type="number" class="form-control" min="10"  max="9999" name="number1" value="" required></div><div class="form-group"><label for="name">Desarrollo de la Plataforma Web:</label><input type="number" class="form-control" min="10"  max="9999" name="number2" value="" required></div><div class="form-group"><label for="name">Despliegue de la Plataforma en el Servidor:</label><input type="number" class="form-control" min="10"  max="9999" name="number3" value="" required></div>' );
						break;
					case "4":
						$("div#div1").empty();
						$("div#div1" ).append( '<div class="form-group"><label for="name">Desarrollo de Tienda virtual:</label><input type="number" class="form-control" min="10"  max="9999" name="number1" value="" required></div><div class="form-group"><label for="name">Diseño personalizado:</label><input type="number" class="form-control" min="10"  max="9999" name="number2" value="" required></div><div class="form-group"><label for="name">Compatibilidad con todo dispositivo:</label><input type="number" class="form-control" min="10"  max="9999" name="number3" value="" required></div><div class="form-group"><label for="name">Metodos de pago</label><input type="number" class="form-control" min="10"  max="9999" name="number4" value="" required></div><div class="form-group"><br></div><div class="form-group"><label for="name">Diseño de logo</label><input type="number" class="form-control" min="10"  max="9999" name="number7" value="" required>	</div><div class="form-group"><label for="name">Propuesta basica SEO:</label><input type="number" class="form-control" min="10"  max="9999" name="number5" value="" required></div>	<div class="form-group"><label for="name">Propuesta mensual SEO:</label><input type="number" class="form-control" min="10"  max="9999" name="number6" value="" required></div>' );
						break;
					case "5":
						$("div#div1").empty();
						$( "div#div1" ).append( '<div class="form-group"><label for="name">Propuesta basica SEO:</label><input type="number" class="form-control" min="10"  max="9999" name="number1" value="" required></div><div class="form-group"><label for="name">Propuesta mensual SEO:</label><input type="number" class="form-control" min="10"  max="9999" name="number2" value="" required></div>' );
						break;

				}

			}).change();

		});

  
  </script>
</body>
</html>