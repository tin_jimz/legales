<?php 
include('docxtemplate.class.php');

if (!empty($_POST)) {

$d = array("Domingo","Lunes","Martes","Miercoles","Jueves","Viernes","Sábado");
$m = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");

            $type = $_POST['type'];      
            $doc = $_POST['doc'];
            $number1 = $_POST['number1'];
            $number2 = $_POST['number2'];
            $number3 = $_POST['number3'];
            $number4 = $_POST['number4'];
            $number5 = $_POST['number5'];
            $number6 = $_POST['number6'];
            $number7 = $_POST['number7'];
            $lenguage =  $_POST['lenguage'];   
            $page = $_POST['page'];
            
         if($lenguage == 'esp'){


            if($type == 'person'){
                $name = 'Estimado '.$_POST['name'].'';
                $name2 = $_POST['name'];
            }
            if($type == 'company'){
                $name = $_POST['name'];
                $name2 = $_POST['name'];
            }

                    if($doc == 1){
                        $total = $number1 + $number2 + $number3;
                        $date =  $d[date('w')]." ".date('d')." de ".$m[date('n')-1]. " del ".date('Y') ;
                        $docx = new DOCXTemplate('standardESP.docx');                
                        $docx->set('name',$name);
                        $docx->set('name2',$name2);
                        $docx->set('date',$date);            
                        $docx->set('number1',$number1);
                        $docx->set('number2',$number2);
                        $docx->set('number3',$number3);
                        $docx->set('total',$total);                
                        $docx->downloadAs('standar.docx');


                    }
                    if($doc == 2){
                        $total = $number1 + $number2 + $number3 + $number4;

                        $date =  $d[date('w')]." ".date('d')." de ".$m[date('n')-1]. " del ".date('Y') ;                
                        $docx = new DOCXTemplate('ecomerceESP.docx');                
                        $docx->set('name',$name);
                        $docx->set('name2',$name2);
                        $docx->set('date',$date);
                        $docx->set('number1',$number1);
                        $docx->set('number2',$number2);
                        $docx->set('number3',$number3);
                        $docx->set('number4',$number4);
                        $docx->set('total',$total);                
                        $docx->downloadAs('ecomerce.docx');


                    }
                    if($doc == 3){
                        $total = $number1 + $number2 + $number3 ;
                        $date =  $d[date('w')]." ".date('d')." de ".$m[date('n')-1]. " del ".date('Y') ;
                        $docx = new DOCXTemplate('customizedESP.docx');                
                        $docx->set('name',$name);
                        $docx->set('name2',$name2);
                        $docx->set('date',$date);
                        $docx->set('page',$page);
                        $docx->set('number1',$number1);
                        $docx->set('number2',$number2);
                        $docx->set('number3',$number3);
                        $docx->set('number4',$number4);
                        $docx->set('total',$total);                
                        $docx->downloadAs('customized.docx');
                        
                    }
                    if($doc == 4){

                        $total = $number1 + $number2 + $number3 + $number4;

                        $date =  $d[date('w')]." ".date('d')." de ".$m[date('n')-1]. " del ".date('Y') ;                
                        $docx = new DOCXTemplate('ecomerceinglesSEO.docx');                
                        $docx->set('name',$name);
                        $docx->set('name2',$name2);
                        $docx->set('date',$date);
                        $docx->set('number1',$number1);
                        $docx->set('number2',$number2);
                        $docx->set('number3',$number3);
                        $docx->set('number4',$number4);
                        $docx->set('number5',$number5);
                        $docx->set('number6',$number6);
                        $docx->set('number7',$number7);
                        $docx->set('total',$total);
                        $docx->downloadAs('ecomerceseo.docx');                        
                    }
                    if($doc == 5){               
                        $docx = new DOCXTemplate('seo.docx');
                        $docx->set('number1',$number1);
                        $docx->set('number2',$number2);                
                        $docx->downloadAs('seo.docx');                        
                    }                 
                }
    
                if($lenguage == 'eng'){

                    if($type == 'person'){
                        $name = 'Dear '.$_POST['name'].'';
                        $name2 = $_POST['name'];
                    }
                    if($type == 'company'){
                        $name = $_POST['name'];
                        $name2 = $_POST['name'];
                    }
            
                    if($doc == 1){
                        $total = $number1 + $number2 + $number3;
                        $date = date('l jS \of F Y');                
                        $docx = new DOCXTemplate('standardENG.docx');
                        $docx->set('name',$name);
                        $docx->set('name2',$name2);
                        $docx->set('date',$date);            
                        $docx->set('number1',$number1);
                        $docx->set('number2',$number2);
                        $docx->set('number3',$number3);
                        $docx->set('total',$total);
                
                        $docx->downloadAs('standar.docx');


                    }
                    if($doc == 2){
                        $total = $number1 + $number2 + $number3 + $number4;
                        $date = date('l jS \of F Y');                
                        $docx = new DOCXTemplate('ecomerceENG.docx');                
                        $docx->set('name',$name);
                        $docx->set('name2',$name2);
                        $docx->set('date',$date);
                        $docx->set('number1',$number1);
                        $docx->set('number2',$number2);
                        $docx->set('number3',$number3);
                        $docx->set('number4',$number4);
                        $docx->set('total',$total);                
                        $docx->downloadAs('ecomerce.docx');


                    }
                    if($doc == 3){

                        $total = $number1 + $number2 + $number3 ;
                        $date = date('l jS \of F Y');                
                        $docx = new DOCXTemplate('customizedENG.docx');                
                        $docx->set('name',$name);
                        $docx->set('name2',$name2);
                        $docx->set('date',$date);
                        $docx->set('page',$page);
                        $docx->set('number1',$number1);
                        $docx->set('number2',$number2);
                        $docx->set('number3',$number3);
                        $docx->set('number4',$number4);
                        $docx->set('total',$total);                
                        $docx->downloadAs('customized.docx');                   
                    }
                    if($doc == 4){

                        $total = $number1 + $number2 + $number3 + $number4;
                        $date = date('l jS \of F Y');                
                        $docx = new DOCXTemplate('ecomerceinglesSEO.docx');                
                        $docx->set('name',$name);
                        $docx->set('name2',$name2);
                        $docx->set('date',$date);
                        $docx->set('number1',$number1);
                        $docx->set('number2',$number2);
                        $docx->set('number3',$number3);
                        $docx->set('number4',$number4);
                        $docx->set('number5',$number5);
                        $docx->set('number6',$number6);
                        $docx->set('number7',$number7);
                        $docx->set('total',$total);
                        $docx->downloadAs('ecomerceseo.docx');                        
                    }
                    if($doc == 5){               
                        $docx = new DOCXTemplate('seo.docx');
                        $docx->set('number1',$number1);
                        $docx->set('number2',$number2);                
                        $docx->downloadAs('seo.docx');                        
                    }   
            }


}

?>