<?php
require_once("../autoload.php");
if(!isset($_SESSION["email"])) {
    redirect("../index.php");
}
  if (isset($_GET["id"])) {
    $id=$_GET["id"];
    $usuarioSeleccionado = Query::showUsers($pdo,'users',$id);
  }
  
  //esto se ejecuta si el usuario indica que desea borrar el usuario
  if (isset($_POST["borrar"])) {
    Query::deleteRow($pdo,'email',$id);
    header('Location:list.php');
    exit;
  //var_dump($_POST);
  }
  elseif (isset ($_POST["no"])) {
    header("Location:list.php");
    exit;
  }
   ?>
 <html lang="es">
<?php include_once '../inc/head.php'; ?>
<body class="hold-transition skin-black sidebar-mini">
<div class="wrapper">
  <!-- Main Header -->
  <?php include_once '../inc/nav-bar.php'; ?>
  <?php include_once '../inc/sidebar.php'; ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->

    <!-- Main content -->
    <section class="content container-fluid">

  
      <form class="" action="" method="post">
        <p>Esta seguro que quiere eliminar este Registro?</p>
        <input type="submit" name="borrar" value="si">
        <input type="submit" name="no" value="no">
        <input type="hidden" name="id" value="<?=$id;?>">
     </form>
  
    
 
     </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <!-- Main Footer -->
  <?php include_once '../inc/main-footer.php'; ?>
  <!-- Add the sidebar's background. This div must be placed
  immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->
<?php include_once '../inc/footer.php'; ?>

</body>
</html>