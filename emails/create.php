<?php
require_once("../autoload.php");
if ($_POST){  

  $email = new Email($_POST["title"],$_POST["content"],$_POST["autor"],$_POST["lenguage"]);
  $errores = $val->verifyEmail($email);
      if(count($errores)==0){        
          $em = BaseMYSQL::createEmail($pdo,$email,'email');
       }      
      $contador = count($_POST["titleem"]);   
      
        for($i=0;$i<$contador;$i++) {
            if(!empty($_POST["titleem"][$i])) {  
              $input = new Input($_POST["titleem"][$i],$_POST["name"][$i],$_POST["type"][$i],$em);
              BaseMYSQL::createInputs($pdo,$input,'email_var');   
            }
        }
        redirect ("list.php");  
    }

 
  
?>
<html lang="es">
<?php include_once '../inc/head.php'; ?>
<body class="hold-transition skin-black sidebar-mini">
<div class="wrapper">
  <!-- Main Header -->
  <?php include_once '../inc/nav-bar.php'; ?>
  <?php include_once '../inc/sidebar.php'; ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <!-- Main content -->

    <style>
      .form-inline {
          margin: 5px 0;
      }    
    </style>
    <section class="content container-fluid">       
     <div class="row">
        <!-- left column -->
        <div class="col-md-10">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h2 class="box-title">Formulario de registro de datos</h2>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <?php
                if(isset($errores)):?>
                  <ul class="alert alert-danger">
                    <?php
                    foreach ($errores as $key => $value) :?>
                      <li> <?=$value;?> </li>
                      <?php endforeach;?>
                  </ul>
                <?php endif;?>
            <form action="" method="POST" enctype= "multipart/form-data"  >
              <div class="box-body">              
                <div class="form-group">
                  <label> Autor:</label>            
                   <input name="autor" type="text" id="autor" class="form-control"  value="<?=$_SESSION["name"];?>" placeholder="Nombre de usuario..."  required/>

                </div>
                  <div class="form-group">
                   <label>Titulo:</label>   
                   <input name="title" type="text" id="title" class="form-control"  value="" placeholder="Titulo del Correo" required/>
       
                </div>              
                <div class="form-group">
                  <label for="exampleInputFile">Contenido</label>
                  <textarea  name="content"  class="form-control" rows="20"></textarea>
                </div>  
                <div class="form-group">
							        <label for="Lenguaje">Lenguaje</label>
                    <select class="form-control"  name="lenguage" id="lenguage" required>
                      <option value="eng">Ing</option>						
                      <option value="esp">Esp</option>
                    </select>
						    </div> 
              <div class="ctn">
                      <div class="form-group">
                        <label> Campos Personalizados:</label>            
                      </div>
                      <div class="form-inline">              
                          <div class="form-group"><input name="name[]" type="text" id="title" class="form-control"  value="" placeholder="Nombre del Campo" /></div> 
                           <div class="form-group"><select class="form-control"  name="type[]" id="type" ><option value="text">Text</option><option value="textarea">Text Area</option><option value="number">number</option><option value="email">email</option></select></div>     
                                  <div class="input-group">
                                        <input type="text" name="titleem[]" class="form-control" placeholder="Label del Campo">
                                        <span class="input-group-btn">
                                          <button class="btn btn-default add_button" type="button"  title="Agregar">+</button>
                                        </span>
                                    </div>
                          </div>

                </div>
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
              <button class="btn-buttom btn-primary" type="submit">Enviar</button>
            
              <button  class="btn-buttom btn-success" type="reset">Restablecer</button>
              </div>
            </form>
          </div>
          <!-- /.box -->
        </div>
      
    </section>
   <!-- /.content -->
   </div>
  <!-- /.content-wrapper -->
  <!-- Main Footer -->
  <?php include_once '../inc/main-footer.php'; ?>
  <!-- Add the sidebar's background. This div must be placed
  immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->
<?php include_once '../inc/footer.php'; ?>


<script>

$(document).ready(function(){
    var x = 1; //Initial field counter is 1
    var maxField = 10; //Input fields increment limitation
    var addButton = $('.add_button'); //Add button selector
    var wrapper = $('.ctn'); //Input field wrapper
      
    $(addButton).click(function(){ //Once add button is clicked
        if(x < maxField){ //Check maximum number of input fields
           x++; //Increment field counter
            $(wrapper).append('<div class="form-inline cp'+x+'"><div class="form-group"><input type="text" name="name[]" class="form-control" placeholder="Nombre del Campo"></div><div class="form-group"><select class="form-control"  name="type[]" id="type" ><option value="text">Text</option><option value="date">Date</option><option value="textarea">Text Area</option><option value="number">number</option><option value="email">email</option></select></div><div class="input-group"> <input name="titleem[]" type="text" id="title" class="form-control"  value="" placeholder="Titulo del Campo" /><span class="input-group-btn"><button class="remove_button btn btn-default " type="button"  title="">-</button></span></div></div></div>'); // Add field html
        }
    });
    $(wrapper).on('click', '.remove_button', function(e){ //Once remove button is clicked
       e.preventDefault();
              $('div.form-inline.cp'+x+'').remove(); //Remove field html
        x--; //Decrement field counter
    });
});

</script>

</body>
</html>