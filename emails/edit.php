<?php
require_once("../autoload.php");
if(!isset($_SESSION["email"])) {
    redirect("../index.php");
}
if (isset($_GET["id"])) {
     $id=$_GET["id"];
     $list =  Query::searchId($pdo,'email',$id);
     $list2 = Query::searchAll($pdo,'email_var','id_email',$id);
    // dd($list2);
}

if (isset($_POST["modificar"])) {

    $id = $_POST['id'];
    $title = $_POST['title'];
    $content = $_POST['content'];
    $autor = $_POST['autor'];
   
    $sql = "UPDATE email SET title = :title, 
            content = :content,
            autor = :autor
      WHERE id = '$id' ";

            $query = $pdo->prepare($sql);
            $query->bindparam(':title', $title);
            $query->bindparam(':content', $content);
            $query->bindparam(':autor', $autor);
            $query->execute();
            header('Location:list.php');


  } elseif (isset($_POST["no"])){
      header('Location:list.php');
      exit;
  }

 ?>
<html lang="es">
<?php include_once '../inc/head.php'; ?>
<body class="hold-transition skin-black sidebar-mini">
<div class="wrapper">
<style>
      .form-inline {
          margin: 5px 0;
      }    
    </style>
  <!-- Main Header -->
  <?php include_once '../inc/nav-bar.php'; ?>
  <?php include_once '../inc/sidebar.php'; ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <!-- Main content -->
    <section class="content container-fluid">   
     <div class="row">
        <!-- left column -->
        <div class="col-md-10">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h2 class="box-title">Formulario de registro de datos</h2>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <?php
                if(isset($errores)):?>
                  <ul class="alert alert-danger">
                    <?php
                    foreach ($errores as $key => $value) :?>
                      <li> <?=$value;?> </li>
                      <?php endforeach;?>
                  </ul>
                <?php endif;?>
    <form class="" action="" method="post">
          <div class="box-body">
              <?php foreach ($list as $key => $value) : ?>
                  <?php foreach ($value as $key1 => $value2) : ?>
                          <div class="form-group">
                                  <?php if($key1 =="id"){?>
                                      <label><?= $key1?> :</label>
                                      <input type="text" class="form-control"  disabled name="<?= $key1?>" value="<?= $value2?>">
                                  <?php }if($key1 =="content"){?>
                                      <label><?= $key1?> :</label>
                                      <textarea  name="<?= $key1?>"  class="form-control" rows="20"><?= $value2?></textarea>
                                 <?php }if($key1 =="lenguage"){?>
                                    <label for="Lenguaje">Lenguaje</label>
                                      <select class="form-control"  name="lenguage" id="lenguage" required>
                                        <?php if($value2 =="eng"){ ?>
                                          <option value="eng" selected>Ing</option>						
                                          <option value="esp">Esp</option>
                                        <?php }if($value2 =="esp"){ ?>
                                          <option value="eng" >Ing</option>						
                                          <option value="esp" selected>Esp</option>
                                        <?php }?>
                                      </select>   
                                  <?php }if($key1 != "id"){              
                                            if($key1 != "password"){
                                              if($key1 != "content"){ 
                                                  if($key1 != "fecha"){ 
                                                    if($key1 != "lenguage"){   ?>
                                                  <label><?= $key1?> :</label>
                                                  <input type="text" class="form-control"  name="<?= $key1?>" value="<?= $value2?>">
                                                <?php }  ?>
                                              <?php }  ?>
                                            <?php }  ?>
                                          <?php }  ?>
                                  <?php }?>   
                              <?php endforeach;?>      
                      <?php endforeach;?>
                      <div class="ctn">

                                  <div class="form-group">
                                    <label> Campos Personalizados:</label>            
                                  </div>
                                  
                                  <?php foreach ($list2 as $key => $value) : ?>

                                    <div class="form-inline">

                                    <?php foreach ($value as $key1 => $value2) : ?>  
                                                  <?php if($key1 =="inputtitle"){ ?>
                                                  <div class="form-group"> 
                                                  
                                                  <input name="name[]" type="text" id="name" class="form-control"  value="<?= $value2?>" placeholder="Nombre del Campo" />
                                                
                                                          
                                                   </div>
                                                   <?php } if($key1 =="inputname"){ ?>
                                                    <div class="form-group">
                                                    <input type="text" class="form-control"   name="titleem[]" value="<?= $value2?> " placeholder="Label del Campo">
                                                     
                                                    </div>     

                                                   <?php  } if($key1 =="type"){?>
                                                  <div class="input-group">
                                                  <select class="form-control"  name="type[]" id="type" >
                                                      <?php if($value2 =="text"){ ?>

                                                        <option value="text" selected>Text</option>
                                                        <option value="textarea">Text Area</option>
                                                        <option value="number">number</option>
                                                        <option value="email">email</option>
                                                        
                                                   <?php  } if($value2 =="textarea"){?>

                                                        <option value="text" >Text</option>
                                                        <option value="textarea" selected>Text Area</option>
                                                        <option value="number">number</option>
                                                        <option value="email">email</option>

                                                        <?php  } if($value2 =="number"){?>
                                                            <option value="text" >Text</option>
                                                            <option value="textarea" >Text Area</option>
                                                            <option value="number" selected>number</option>
                                                            <option value="email">email</option>
                                                          <?php  } if($value2 =="email"){?>
                                                            <option value="text" >Text</option>
                                                            <option value="textarea" >Text Area</option>
                                                            <option value="number" >number</option>
                                                            <option value="email" selected>email</option>

                                                    <?php }  ?>
                                                      </select>
                                                      <span class="input-group-btn">
                                                          <button class="btn btn-default add_button" type="button"  title="Agregar">+</button>
                                                        </span>                                                  
                                                  </div> 
                                                 <?php }  ?>

                                    <?php endforeach;?>

                                    </div>          


                                <?php endforeach;?> 

                   
                                
                      </div>
            </div>
          <br>
          <div class="box-footer">
          <p>Esta seguro que quieres modificar este registro?</p>
            <input class="btn-buttom btn-primary"type="submit" name="modificar" value="si">
            <input class="btn-buttom btn-primary" type="submit" name="no" value="no">
            <input type="hidden" name="id" value="<?=$id;?>">
          </div> 
   </form>
   </div>
          <!-- /.box -->
        </div>

      
    </section>
   <!-- /.content -->
   </div>
  <!-- /.content-wrapper -->
  <!-- Main Footer -->
  <?php include_once '../inc/main-footer.php'; ?>
  <!-- Add the sidebar's background. This div must be placed
  immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->
<?php include_once '../inc/footer.php'; ?>
<script>

$(document).ready(function(){
    var x = 1; //Initial field counter is 1
    var maxField = 10; //Input fields increment limitation
    var addButton = $('.add_button'); //Add button selector
    var wrapper = $('.ctn'); //Input field wrapper
      
    $(addButton).click(function(){ //Once add button is clicked
        if(x < maxField){ //Check maximum number of input fields
           x++; //Increment field counter
            $(wrapper).append('<div class="form-inline cp'+x+'"><div class="form-group"><input type="text" name="name[]" class="form-control" placeholder="Nombre del Campo"></div><div class="form-group"><select class="form-control"  name="type[]" id="type" ><option value="text">Text</option><option value="textarea">Text Area</option><option value="number">number</option><option value="email">email</option></select></div><div class="input-group"> <input name="titleem[]" type="text" id="title" class="form-control"  value="" placeholder="Titulo del Campo" /><span class="input-group-btn"><button class="remove_button btn btn-default " type="button"  title="">-</button></span></div></div></div>'); // Add field html
        }
    });
    $(wrapper).on('click', '.remove_button', function(e){ //Once remove button is clicked
       e.preventDefault();
              $('div.form-inline.cp'+x+'').remove(); //Remove field html
        x--; //Decrement field counter
    });
});


</script>
</body>
</html>
