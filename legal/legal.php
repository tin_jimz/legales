<?php
require_once("../autoload.php");
if(!isset($_SESSION["email"])) {
    redirect("index.php");
}
?>
<html lang="es">
<?php include_once '../inc/head.php'; ?>
<body class="hold-transition skin-black sidebar-mini">
<div class="wrapper">
  <!-- Main Header -->
  <?php include_once '../inc/nav-bar.php'; ?>
  <?php include_once '../inc/sidebar.php'; ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->

    <!-- Main content -->
    <section class="content container-fluid">

	
	<div class="content">
		<div class="row">
				<div class="col-md-4">
				</div>
				<div class="col-md-4">
					<img src="../imagenes/logogtconnections.png"  width=100%; />
						<form name="formulario" method="post" action="script.php">
						<div class="form-group">
							<label for="name">Nombre empresa o Dueño:</label>
							<input type="text" class="form-control" name="name" value="">
						</div>
						<div class="form-group">
							<label for="name">Url del sitio:</label>
							<input type="text" class="form-control" name="url" value="">
						</div>
						<div class="form-group">
							<label for="Lenguaje">Lenguaje</label>
							<select class="form-control"  name="lenguage" id="lenguage">
								<option value="esp">Esp</option>
								<option value="eng">Eng</option>						
							</select>
						</div>			 
							<input type="submit" value="Generar" class="btn btn-default" />
						</form>
				</div>
					<div class="col-md-4">
					</div>
	</div>
</div>
		
</section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <!-- Main Footer -->
  <?php include_once '../inc/main-footer.php'; ?>
  <!-- Add the sidebar's background. This div must be placed
  immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->
<?php include_once '../inc/footer.php'; ?>

</body>
</html>