<?php 
include('docxtemplate.class.php');

if (!empty($_POST)) {
$dias = array("Domingo","Lunes","Martes","Miercoles","Jueves","Viernes","Sábado");
$meses = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");

$name = $_POST['name'];
$url =$_POST['url'];
$fecha =  $dias[date('w')]." ".date('d')." de ".$meses[date('n')-1]. " del ".date('Y') ;

$lenguage =  $_POST['lenguage'];
    

    if($lenguage == 'esp'){
        $docx = new DOCXTemplate('legal.docx');
        $docx->set('Name', $name );
        $docx->set('url', $url);
        $docx->downloadAs('legales.docx');
    }
    
    if($lenguage == 'eng'){
        $docx = new DOCXTemplate('legalENG.docx');
        $docx->set('Name', $name );
        $docx->set('url', $url);
        $docx->downloadAs('legales.docx');
    }


}

?>